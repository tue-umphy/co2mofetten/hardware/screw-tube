# 🔩 Screw Tube

This is a customizable design of tube segments that can be screwed together.

## 🏗 Building the Files

You can either open the `*.scad` file with [OpenSCAD](https://openscad.org) and render the design there (F6, then F7), or you can use `scons` to build it automatically with nice filenames including the current revision number:

```bash
# Run 'scons' to build all files (rendered images of all parameter sets, STL files for printing, etc...)
scons

# Run 'scons -kj10' to use 10 processes in parallel (much faster!)
scons -kj10

# Run 'scons -n' to just see what would be built
scons -n

# Just build a single file (e.g. one extracted from the output of 'scons -n'):
scons screw-tube.scad-0258812-2-5cm-Test-Slice.png
```


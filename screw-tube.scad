// clang-format off
use <threads-scad/threads.scad>;
// clang-format on

/* [Dimensions of Screw Thread] */
height_screw_thread = 20;       // [5:0.1:60]
diameter_screw_thread = 65;     // [5:0.1:140]
pitch_screw_thread = 4;         // [0:1:5]
tooth_angle_screw_thread = 40;  // [20:1:60]
tip_height_screw_thread = 0;    // [0:0.1:20]
tip_min_fract_screw_thread = 0; // [0:0.1:1]

/* [Dimensions of Screw Hole] */
height_screw_hole = 20;       // [5:0.1:60]
diameter_screw_hole = 65;     // [5:0.1:140]
pitch_screw_hole = 4;         // [0:1:5]
tooth_angle_screw_hole = 40;  // [20:1:60]
tip_height_screw_hole = 0;    // [0:0.1:20]
tip_min_fract_screw_hole = 0; // [0:0.1:1]

/* [Dimensions of Tube] */
height_tube = 100;          // [20:1:250]
height_tube_transition = 5; // [0:0.1:10]
diameter_tube = 70;         // [15:0.1:150]

/* [Dimensions of inner Tube] */
inner_tube_diameter = 55; //[0:0.1:145]
inner_tube_height = (height_tube + height_screw_thread + 5);

/* [Display] */
cut_tube_in_preview = false;

$fs = $preview ? 3 : 0.5;
$fa = $preview ? 5 : 1;
epsilon = 0.1 * 1;

intersection()
{
  difference()
  {
    union()
    {
      ScrewHole(outer_diam = diameter_screw_hole,
                height = height_screw_hole,
                pitch = pitch_screw_hole,
                tooth_angle = tooth_angle_screw_hole,
                tip_height = tip_height_screw_hole,
                tip_min_fract = tip_min_fract_screw_hole) hull()
      {
        cylinder(d = diameter_tube, h = height_tube - height_tube_transition);
        translate([ 0, 0, height_tube - height_tube_transition ])
        {
          translate([ 0, 0, height_tube_transition - epsilon ])
            linear_extrude(epsilon) projection(cut = true)
              ScrewThread(outer_diam = diameter_screw_thread,
                          height = height_screw_thread,
                          pitch = pitch_screw_thread,
                          tooth_angle = tooth_angle_screw_thread,
                          tip_height = tip_height_screw_thread,
                          tip_min_fract = tip_min_fract_screw_thread);
        }
      }
      translate([ 0, 0, height_tube ])
      {
        render(convexity = 10)
          ScrewThread(outer_diam = diameter_screw_thread,
                      height = height_screw_thread,
                      pitch = pitch_screw_thread,
                      tooth_angle = tooth_angle_screw_thread,
                      tip_height = tip_height_screw_thread,
                      tip_min_fract = tip_min_fract_screw_thread);
      }
    }
    translate([ 0, 0, -epsilon ])
    {
      cylinder(d = inner_tube_diameter, h = inner_tube_height);
    }
  }
  if (cut_tube_in_preview && $preview)
    translate([ 0, 0, -epsilon / 2 ]) cube([
      diameter_tube,
      diameter_tube,
      height_tube + height_screw_thread +
      epsilon
    ]);
}
